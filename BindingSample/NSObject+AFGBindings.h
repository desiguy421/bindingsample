//
//  NSObject+AFGBindings.h
//  BindingSample
//
//  Created by Aqeel Gunja on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, AFGBindingTransformType)
{
    AFGBindingTransformTypeAlwaysString,
    AFGBindingTransformTypeStringToNumber,
    AFGBindingTransformTypeNone
};

typedef id(^AFGTransformBlock)(id value);

@interface NSObject (AFGBindings)


- (void)afg_bind:(NSString *)binding toObject:(id)object withKeyPath:(NSString*)keyPath transformType:(AFGBindingTransformType)transformType;

// Passing nil for transformBlock will transform all values to NSString* by default.
- (void)afg_bind:(NSString *)binding toObject:(id)object withKeyPath:(NSString*)keyPath transformBlock:(AFGTransformBlock)transformBlock;

- (void)afg_unbind:(NSString *)binding;

@end
