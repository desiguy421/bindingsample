//
//  BNDViewController.m
//  BindingSample
//
//  Created by Aqeel Gunja on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BNDViewController.h"
#import "NSObject+AFGBindings.h"
#import "AFGNumberTransformer.h"

@interface BNDViewController ()

@property (nonatomic,strong) IBOutlet UISlider *slider;
@property (nonatomic,strong) IBOutlet UITextField *textField;
@property (nonatomic,strong) IBOutlet UILabel *counterLabel;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic,strong) IBOutlet UISwitch *activitySwitch;

@property (nonatomic,assign) float progress;
@property (nonatomic,assign) NSInteger counter;
@property (nonatomic,assign) BOOL activityIsAnimating;

@end

@implementation BNDViewController

@synthesize slider;
@synthesize progress;
@synthesize textField;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)valueChanged:(id)sender {
    UISlider *aSlider = sender;
    self.progress = [aSlider value];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self.textField afg_bind:@"text" toObject:self withKeyPath:@"progress" transformBlock:^id(id value)
    {
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:[value doubleValue]];
        return [NSString stringWithFormat:@"%@", date];
    }];
    self.counterLabel.text = nil;
    
    [self.counterLabel afg_bind:@"text" toObject:self withKeyPath:@"counter" transformType:AFGBindingTransformTypeAlwaysString];
    
    NSTimer *timer = [NSTimer timerWithTimeInterval:1.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)timerFired:(NSTimer *)timer
{
    self.counter++;
}

- (void)viewDidUnload
{
    [self.textField afg_unbind:@"text"];
    [self afg_unbind:@"progress"];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (BOOL)textFieldShouldReturn:(UITextField *)aTextField {
    [aTextField resignFirstResponder];
    return YES;
}

@end
