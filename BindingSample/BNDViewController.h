//
//  BNDViewController.h
//  BindingSample
//
//  Created by Aqeel Gunja on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNDViewController : UIViewController <UITextFieldDelegate>

@end
