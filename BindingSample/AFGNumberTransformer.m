//
//  AFGNumberTransformer.m
//  BindingSample
//
//  Created by Aqeel Gunja on 12/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "AFGNumberTransformer.h"

@implementation AFGNumberTransformer {
    NSNumberFormatter *numberFormatter;
}

+ (BOOL)allowsReverseTransformation {
    return YES;
}

+ (Class)transformedValueClass {
    return [NSNumber class];
}

- (id)init {
    return [self initWithNumberStyle:NSNumberFormatterDecimalStyle];
}

- (id)initWithNumberStyle:(NSNumberFormatterStyle)numberStyle {
    if ((self = [super init])) {
        numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setNumberStyle:numberStyle];
    }
    return self;    
}

- (NSNumberFormatter *)numberFormatter {
    return numberFormatter;
}

- (id)transformedValue:(id)value {
    return [numberFormatter numberFromString:value];
}

- (id)reverseTransformedValue:(id)value {
    return [numberFormatter stringFromNumber:value];
}

@end
