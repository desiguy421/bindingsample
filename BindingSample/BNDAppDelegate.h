//
//  BNDAppDelegate.h
//  BindingSample
//
//  Created by Aqeel Gunja on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BNDViewController;

@interface BNDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) BNDViewController *viewController;

@end
