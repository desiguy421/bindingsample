//
//  AFGNumberTransformer.h
//  BindingSample
//
//  Created by Aqeel Gunja on 12/22/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFGNumberTransformer : NSValueTransformer

- (id)initWithNumberStyle:(NSNumberFormatterStyle)numberStyle;

@property (nonatomic,readonly) NSNumberFormatter *numberFormatter;

@end
