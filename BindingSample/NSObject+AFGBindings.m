//
//  NSObject+AFGBindings.m
//  BindingSample
//
//  Created by Aqeel Gunja on 12/21/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "NSObject+AFGBindings.h"
#import <objc/runtime.h>

NSString * const AFGNSValueTransformerBindingOption = @"AFGNSValueTransformerBindingOption";
NSString * const AFGNSValueTransformerNameBindingOption = @"AFGNSValueTransformerNameBindingOption";

@interface AFGBindingObserver : NSObject {
@private
    NSDictionary *options;
    id receiverObject;
    NSString *receiverKeyPath;
    id boundedObject;
    NSString *boundedKeyPath;
}

@property (nonatomic, copy) AFGTransformBlock transformBlock;
@property (nonatomic, assign) AFGBindingTransformType transformType;

@end

@implementation AFGBindingObserver

static NSString * const BoundingContext = @"BoundingContext";

- (void)addObservers
{
    [boundedObject addObserver:self forKeyPath:boundedKeyPath options:NSKeyValueObservingOptionNew context:(__bridge void *)BoundingContext];
}

- (void)removeObservers
{
    [boundedObject removeObserver:self forKeyPath:boundedKeyPath];
}

- (id)initWithReceiverObject:(id)aReceiverObject receiverKeyPath:(NSString *)aReceiverKeyPath boundedObject:(id)aBoundedObject boundedKeyPath:(NSString *)aBoundedKeyPath transformBlock:(AFGTransformBlock)transformBlock transformType:(AFGBindingTransformType)transformType
{
    if ((self = [super init])) {
        receiverObject = aReceiverObject;
        receiverKeyPath = [aReceiverKeyPath copy];
        boundedObject = aBoundedObject;
        boundedKeyPath = [aBoundedKeyPath copy];
        _transformBlock = [transformBlock copy];
        _transformType = transformType;
        [self addObservers];
    }
    return self;
}

- (void)dealloc
{
    [self removeObservers];
}

- (id)transformedValueOfValue:(id)value
{
    if (self.transformBlock)
    {
        return self.transformBlock(value);
    }
    else if (self.transformType == AFGBindingTransformTypeAlwaysString)
    {
        return [NSString stringWithFormat:@"%@", value];
    }
    else if (self.transformType == AFGBindingTransformTypeStringToNumber &&
             [value isKindOfClass:[NSString class]])
    {
        return @([value doubleValue]);
    }
    else
    {
        return value;
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    //NSValueTransformer *transformer = [options objectForKey:AFGNSValueTransformerBindingOption];
    if (context == (__bridge void *)BoundingContext)
    {
        id value = [change objectForKey:NSKeyValueChangeNewKey];
        id receiverValue = [receiverObject valueForKeyPath:receiverKeyPath];
        value = [self transformedValueOfValue:value];

        if (value && ![value isEqual:receiverValue]) {
            [receiverObject setValue:value forKeyPath:receiverKeyPath];
        }
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

@end

static NSString *AFGBindingMapKey = @"AFGBindingMapKey";

@implementation NSObject (AFGBindings)


- (NSMutableDictionary *)afg_bindingMap 
{
    NSMutableDictionary *bindingMap = objc_getAssociatedObject(self, (__bridge const void *)AFGBindingMapKey);
    if (!bindingMap) {
        bindingMap = [[NSMutableDictionary alloc] init];
        objc_setAssociatedObject(self, (__bridge const void *)AFGBindingMapKey, bindingMap, OBJC_ASSOCIATION_RETAIN);
    }
    return bindingMap;
}

- (NSMutableSet *)afg_bindingSetForBinding:(NSString *)binding 
{
    NSMutableDictionary *bindingMap = [self afg_bindingMap];
    NSMutableSet *bindingSet = [bindingMap objectForKey:binding];
    if (!bindingSet) {
        bindingSet = [[NSMutableSet alloc] init];
        [bindingMap setValue:bindingSet forKey:binding];
    }
    return bindingSet;
}

- (void)afg_bind:(NSString *)binding toObject:(id)object withKeyPath:(NSString*)keyPath transformType:(AFGBindingTransformType)transformType
{
    NSMutableSet *bindingSet = [self afg_bindingSetForBinding:binding];
    AFGBindingObserver *bindingObserver = [[AFGBindingObserver alloc] initWithReceiverObject:self receiverKeyPath:binding boundedObject:object boundedKeyPath:keyPath transformBlock:nil transformType:transformType];
    bindingObserver.transformType = transformType;
    [bindingSet addObject:bindingObserver];
}

- (void)afg_bind:(NSString *)binding toObject:(id)object withKeyPath:(NSString*)keyPath transformBlock:(AFGTransformBlock)transformBlock
{
    NSMutableSet *bindingSet = [self afg_bindingSetForBinding:binding];
    AFGBindingObserver *bindingObserver = [[AFGBindingObserver alloc] initWithReceiverObject:self receiverKeyPath:binding boundedObject:object boundedKeyPath:keyPath transformBlock:transformBlock transformType:AFGBindingTransformTypeNone];
    [bindingSet addObject:bindingObserver];
}

- (void)afg_unbind:(NSString *)binding 
{
    NSMutableSet *bindingSet = [self afg_bindingSetForBinding:binding];
    [bindingSet removeAllObjects];
}

@end
